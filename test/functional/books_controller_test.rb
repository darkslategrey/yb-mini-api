require 'test_helper'

class BooksControllerTest < ActionController::TestCase
  include Devise::TestHelpers
  # include Warden::Test::Helpers
  # Warden.test_mode!

  setup do
    @book = books(:one)
    sign_in users(:one)
  end

  test 'creation' do
    assert_equal @book.name, "goedel, esher et bach"
  end

  test "should get index" do
    get :index
    assert_response :success
    assert_not_nil assigns(:books)
  end

  test "should get new" do
    get :new
    assert_response :success
  end

  test "should create book" do
    assert_difference('Book.count') do
      post :create, book: { name: "1984" }
    end
    assert_equal "1984", JSON.parse(response.body)["name"]
  end

  test "should show book" do
    get :show, id: @book
    assert_equal "src_1", JSON.parse(response.body).fetch("source")
  end

  test "should get edit" do
    get :edit, id: @book
    assert_response :success
  end

  test "should update book" do
    put :update, id: @book, book: Book.first
    assert_redirected_to book_path(assigns(:book))
  end

  test "should destroy book" do
    assert_difference('Book.count', -1) do
      delete :destroy, id: @book
    end

    assert_redirected_to books_path
  end
end
