class CreateBookReadings < ActiveRecord::Migration
  def change
    create_table :book_readings do |t|
      t.integer :book_id

      t.timestamps
    end
  end
end
