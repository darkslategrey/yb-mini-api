class CreateBooks < ActiveRecord::Migration
  def change

    create_table "books", :force => true do |t|
      t.string "name"
      t.string "author"
      t.datetime "created_at"
      t.datetime "updated_at"
      t.integer "editor_id"
      t.string "cover_file_name"
      t.string "cover_content_type"
      t.string "cover_file_size"
      t.integer "total_pages"
      t.string "cover_url"
      t.string "validation_state", :default => "waiting_for_creation"
      t.text "blurb"
      t.boolean "premium", :default => false
      t.integer "summary_page"
      t.date "publication_date"
      t.float "average_rating", :default => 0.0
      t.integer "number_of_comments", :default => 0
      t.integer "number_of_readers", :default => 0
      t.string "isbn"
      t.string "ean"
      t.text "author_bio"
      t.datetime "removal_date"
      t.text "headline"
      t.string "affiliate_link", :default => "", :null => false
      t.string "interview_link"
      t.string "creation_state", :default => "creation_basic"
      t.boolean "pdf_slicing_success", :default => false, :null => false
      t.integer "word_count"
      t.integer "character_count"
      t.text "excluded_country_list", :default => "", :null => false
      t.string "epub_file_name"
      t.boolean "epub_slicing_success", :default => false, :null => false
      t.text "pdf_crypt_key", :default => "", :null => false
      t.text "pdf_crypt_iv", :default => "", :null => false
      t.datetime "visible_at"
      t.datetime "withdrawn_at"
      t.text "epub_package"
      t.text "epub_crypt_key", :default => "", :null => false
      t.text "epub_crypt_iv", :default => "", :null => false
      t.text "authors_names"
      t.integer "epub_file_size", :default => 0, :null => false
      t.integer "epub_sliced_size", :limit => 8, :default => 0, :null => false
      t.float "score", :default => 1.0, :null => false
      t.float "score_boost", :default => 1.0, :null => false
      t.string "identifier"
      t.string "source"
      t.integer "epub_support_level", :default => 0, :null => false
      t.integer "volume_number"
      t.integer "serie_id"
      t.decimal "remuneration_ratio", :precision => 8, :scale => 2, :default => 1.0, :null => false
      t.text "languages", :default => [], :null => false, :array => true
      t.string "bisac", :default => [], :null => false, :array => true
      t.string "clil", :default => [], :null => false, :array => true
      t.string "categories_mapping", :default => [], :null => false, :array => true
      t.text "keywords", :default => [], :null => false, :array => true
      t.string "epub_md5"
      t.string "pdf_md5"
      t.string "cover_md5"
      t.boolean "editable", :default => true, :null => false
      t.text "distribution_platform_meta"
      t.string "cover_color", :limit => 7
      t.integer "likes_count", :default => 0, :null => false
      t.integer "price_id"
      t.string "ojd_id"
    end

    add_index "books", ["categories_mapping"], :name => "index_books_on_categories_mapping", :using => :gin
    add_index "books", ["editor_id"], :name => "index_books_on_editor_id"
    add_index "books", ["epub_support_level", "validation_state", "id"], :name => "books_epub_support_level_validation_state_id_idx"
    add_index "books", ["epub_support_level", "validation_state"], :name => "books_expr_epub_support_level_validation_state_idx", :where => "((((validation_state)::text = 'visible'::text) AND (epub_support_level > 0)) AND (epub_support_level <= 2))"
    add_index "books", ["id"], :name => "books_expr_id_idx", :where => "(((epub_support_level > 0) AND (epub_support_level <= 2)) AND ((validation_state)::text = 'visible'::text))"
    add_index "books", ["id"], :name => "books_id_expr_idx", :where => "(((epub_support_level > 0) AND (epub_support_level <= 2)) AND ((validation_state)::text = 'visible'::text))"
    add_index "books", ["serie_id"], :name => "index_books_on_serie_id"

    create_table "books_selections", :force => true do |t|
      t.integer "book_id", :null => false
      t.integer "selection_id", :null => false
      t.integer "order_position", :default => 99999, :null => false
      t.datetime "created_at", :null => false
      t.datetime "updated_at", :null => false
    end

    add_index "books_selections", ["selection_id", "book_id"], :name => "books_selections_selection_id_book_id_idx", :unique => true
    add_index "books_selections", ["selection_id", "order_position"], :name => "books_selections_selection_id_order_position_idx", :order => {"order_position" => :desc}

    create_table "catalogs_books", :id => false, :force => true do |t|
      t.integer "catalog_id"
      t.integer "book_id"
      t.date "created_at"
    end

    add_index "catalogs_books", ["catalog_id", "book_id"], :name => "index_catalogs_books_on_catalog_id_and_book_id", :unique => true

    create_table "categories", :force => true do |t|
      t.string "name", :null => false
      t.integer "order_position", :default => 99999, :null => false
      t.string "bisac", :default => [], :null => false, :array => true
      t.string "clil", :default => [], :null => false, :array => true
      t.string "mapping", :default => [], :null => false, :array => true
      t.datetime "created_at", :null => false
      t.datetime "updated_at", :null => false
      t.boolean "is_adult", :default => false, :null => false
      t.string "img_landscape_1024_file_name"
      t.string "img_landscape_1024_content_type"
      t.integer "img_landscape_1024_file_size"
      t.datetime "img_landscape_1024_updated_at"
      t.string "img_square_485_file_name"
      t.string "img_square_485_content_type"
      t.integer "img_square_485_file_size"
      t.datetime "img_square_485_updated_at"
      t.integer "parent_category_id"
      t.string "img_landscape_568_file_name"
      t.string "img_landscape_568_content_type"
      t.integer "img_landscape_568_file_size"
      t.datetime "img_landscape_568_updated_at"
      t.integer "sub_application_id", :null => false
    end

    add_index "categories", ["is_adult"], :name => "categories_is_adult_idx", :where => "(is_adult = true)"
    add_index "categories", ["parent_category_id", "name"], :name => "index_categories_on_parent_category_id_and_name", :unique => true
    add_index "categories", ["sub_application_id"], :name => "index_categories_on_sub_application_id"


  end
end
