class RequestContext
  attr_reader :content_provider, :users, :client_application, :permissions,
              :sub_application, :current_user

  extend Forwardable
  def_delegators :@client_application, :device

  def initialize(options = {})
    @content_provider   = options[:content_provider]
    @users              = options[:users]
    @client_application = options[:client_application]
    @permissions        = options[:permissions]
    @sub_application    = options[:sub_application]
    @request            = options[:request]
    @current_user       = options[:current_user]
  end


end
