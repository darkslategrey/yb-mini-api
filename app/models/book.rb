# encoding: utf-8
require 'tempfile'
require 'open-uri'

class Book < ActiveRecord::Base
  extend Forwardable

  belongs_to :editor
  belongs_to :serie

  has_many :book_readings, :dependent => :destroy
  has_many :user_ipads, :through => :book_readings
  has_many :page_readings,  :through => :book_readings
  has_many :comments, :dependent => :destroy
  has_many :reviews
  has_many :authors_books, :dependent => :destroy
  has_many :authors, :through => :authors_books , :uniq => true
  has_many :books_selections
  has_many :selections, :through => :books_selections
  has_many :recommendations, :dependent => :destroy


  attr_protected :validation_state, :creation_state

  def_delegators :cover_thumb_url, :bucket_directory


  ################## SEARCH ##################

  paginates_per 18

  scope :visibles, lambda { where(:validation_state => 'visible') }
  scope :premium, lambda { where(:premium => true) }
  scope :freemium, lambda { where(:premium => false) }
  scope :filter, lambda { |filter, options = {}| filter.book_filter(self, options) if filter}
  scope :score, lambda { order("books.score * books.score_boost DESC") }
  scope :select_default_api_fields, lambda { select('books.id, books.blurb, books.name, books.premium, books.total_pages, books.average_rating, books.number_of_comments, books.epub_slicing_success, books.epub_support_level, books.editor_id, extract(epoch from books.updated_at)::integer AS version, books.languages, books.epub_sliced_size, books.publication_date, books.cover_color, books.likes_count, bp.value AS price, books.ojd_id').joins('LEFT JOIN book_prices bp ON bp.id = books.price_id') }
  scope :without_adult_content, lambda { BookFilters::NoPorn.new.reduce(self) }
  scope :api_top, lambda { without_adult_content.score }

  #Search book by ids in original order
  def self.find_as_sorted(ids)
    if ids.length > 0
      values = []
      ids.each_with_index do |id, index|
        values << "(#{id}, #{index + 1})"
      end
      self.joins("JOIN (VALUES #{values.join(",")}) as x (id, ordering) ON #{table_name}.id = x.id").order('x.ordering')
    else
      self
    end
  end

  def categories
    Category.joins('INNER JOIN books_categories bc ON bc.category_id = categories.id').
      where('bc.book_id = ?', self.id)
  end

  def epub_toc
    if self.epub_package
      package = JSON.parse(self.epub_package)
      if package and package["toc"]
        return package["toc"]
      end
    end
    return {"childrens" => []}
  end

  def visible?
    self.validation_state == 'visible'
  end


end

