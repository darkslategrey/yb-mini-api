class Category < ActiveRecord::Base
  class CategoryWithAPIFields < Category
    default_scope { select_api_fields }
  end
  attr_accessible :img_landscape_1024, :img_landscape_568, :img_square_485, :name, :bisac, :clil, :order_position, :is_adult, :bisac_list, :clil_list

  has_many :children, foreign_key: 'parent_category_id',
                      class_name: 'Category'
  has_many :children_with_api_fields, foreign_key: 'parent_category_id',
                                      class_name: 'CategoryWithAPIFields'
  belongs_to :parent, foreign_key: 'parent_category_id',
                      class_name: 'Category'
  belongs_to :sub_application


  scope :ordered, -> { order("order_position,created_at ASC") }
  scope :roots, -> { where('parent_category_id IS NULL') } # ::parents is already defined
  scope :children_of, -> (parent_id) { where('parent_category_id = ?', parent_id) }
  scope :select_api_fields, -> { select('id, name, img_landscape_1024_file_name, img_landscape_568_file_name, img_square_485_file_name, extract(epoch from categories.updated_at)::integer AS version, order_position') }

  before_save :assign_code_mapping

  def img_landscape_1024_url
    image_url("categories/#{self.id}/img/img_landscape_1024.jpg")
  end

  def img_landscape_568_url
    image_url("categories/#{self.id}/img/img_landscape_568.jpg")
  end

  def img_square_485_url
    image_url("categories/#{self.id}/img/img_square_485.jpg")
  end

  #def image_url(path)
  #  YB::Core.cdn.library.unsigned_url(obj: path, options: { version: self.read_attribute_before_type_cast('version') })
  #end

  def series
    Serie.joins('INNER JOIN series_categories sc ON sc.serie_id = series.id AND sc.category_id = %i' % self.id)
  end


  def books
    Book.joins('INNER JOIN books_categories bc ON bc.book_id = books.id AND bc.category_id = %i' % self.id)
  end

  #Take an arry of category names and return BISAC codes
  def self.bisac_from_names(names)
    names.map do |name|
      cat = Category.where("LOWER(name) = LOWER(?)", name).first
      if cat
        cat.bisac.slice(0, 1)
      else
        nil
      end
    end.flatten.compact
  end

  def bisac_list
    self.bisac.join(',')
  end

  def bisac_list=(val)
    self.bisac = val.split(',')
  end

  def clil_list
    self.clil.join(',')
  end

  def clil_list=(val)
    self.clil = val.split(',')
  end

  private
  def assign_code_mapping
    mapping = []
    bisacs = self.bisac.map do |bisac|
      mapping << "bisac.#{bisac.downcase}"
      bisac.upcase
    end
    clils = self.clil.map do |clil|
      mapping << "clil.#{clil}"
      clil
    end
    self.bisac = bisacs.uniq
    self.clil = clils.uniq
    self.mapping = mapping.uniq
  end
end
