class CategoryParentSerializer < Serializer
  def serialize(category, visitor)
    if category.parent
      visitor.nested(:parent) do |parent_visitor|
        parent(category, parent_visitor)
      end
    end
  end

  private

  def parent(category, visitor)
    serializer = CategorySerializer.new(@context)
    serializer.serialize(category.parent, visitor)
  end
end

