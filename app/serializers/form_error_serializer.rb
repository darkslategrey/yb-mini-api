class FormErrorSerializer < Serializer
  def serialize(error, visitor)
    visitor.nested(:error) do |error_visitor|
      error_visitor[:code]    = error.code
      error_visitor[:message] = error.message

      error_visitor.nested(:fields) do |fields_visitor|
        error.fields.each do |key, value|
          fields_visitor[key] = value
        end
      end
    end
  end
end
