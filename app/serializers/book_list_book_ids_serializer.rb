class BookListBookIdsSerializer < Serializer
  def serialize(book_list, visitor)
    visitor[:book_ids] = book_ids(book_list)
  end

  private

  def book_ids(book_list)
    @book_ids ||= book_list.
      book_list_books.
      order('book_list_books.added_at DESC, book_list_books.book_id ASC').
      pluck(:book_id)
  end
end

