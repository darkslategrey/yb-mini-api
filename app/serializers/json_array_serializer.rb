require 'oj'

class JSONArraySerializer
  def initialize(serializers, options = {})
    @serializers = serializers
    @options     = options
  end

  def serialize(objects)
    visitors = objects.map do |object|
      JSONSerializerVisitor.new.tap do |visitor|
        @serializers.each do |serializer|
          serializer.serialize(object, visitor)
        end
      end
    end

    result = if @options[:root]
               { @options[:root] => visitors }
             else
               visitors
             end

    Oj.dump(result, mode: :compat)
  end
end

