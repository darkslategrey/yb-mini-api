# coding: utf-8
class BookSerializer < Serializer
  def serialize(book, visitor)
     visitor[:id]                 = book.id
     visitor[:blurb]              = blurb(book)
     visitor[:name]               = name(book)
     visitor[:premium]            = book.premium

     visitor[:total_pages]        = book.total_pages
     visitor[:average_rating]     = book.average_rating
     visitor[:number_of_comments] = book.number_of_comments
     visitor[:format_epub]        = format_epub(book)
#     visitor[:thumbnail_342_url]  = book.cover_thumb_url('252x342'.freeze)
#     visitor[:thumbnail_200_url]  = book.cover_thumb_url('130x200'.freeze)
#     visitor[:thumbnail_143_url]  = book.cover_thumb_url('102x143'.freeze)
#     visitor[:thumbnail_590_url]  = book.cover_thumb_url('443x590'.freeze)
     visitor[:cover_color]        = book.cover_color
  #   visitor[:share_url]          = share_url(book)
  #   visitor.nested(:editor) do |editor_visitor|
  #     editor(book, editor_visitor)
  #   end
   #  visitor[:authors]            = authors(book, visitor)
     visitor[:publication_date]   = publication_date(book)
   #  visitor[:file_size]          = book.epub_sliced_size
     #  visitor[:languages]          = languages(book)
     visitor[:likes_count]        = book.likes_count
     visitor[:price]              = price(book)
  #   visitor[:ojd_url]            = ojd_url(book)
  end

  private

  def publication_date(book)
    book.publication_date.try(:iso8601)
  end


  def format_epub(book)
    if book.epub_slicing_success && @context.device.epub_support_level
      return @context.device.epub_support_level >= book.epub_support_level
    else
      return false
    end
  end

  def name(book)
    ActionController::Base.helpers.strip_tags(coder.decode(book.name))
  end

  def blurb(book)
    ActionController::Base.helpers.strip_tags(coder.decode(book.blurb))
  end

  def coder
    @@coder ||= HTMLEntities.new
  end


  def languages(book)
    book.languages.map { |code| t("language_codes.#{code}") }
  end

  def price(book)
    if value = book.read_attribute_before_type_cast('price')
      value          = value.to_f
      value_per_page = (value / book.total_pages).round(3)
      "#{value} € (soit #{value_per_page} € par page lue)"
    end
  end


end

class ShowBookSerializer < BookSerializer

  def serialize(book, visitor)
    super(book, visitor)
    visitor[:source] = book.source
  end
end


