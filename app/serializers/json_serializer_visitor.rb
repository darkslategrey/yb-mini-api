require 'oj'
require 'forwardable'

class JSONSerializerVisitor
  extend Forwardable
  def_delegators :@attributes, :[]=, :[]

  def initialize
    @attributes = {}
  end

  def nested(key = nil)
    visitor = if key
                @attributes[key] ||= self.class.new
              else
                self.class.new
              end

    yield visitor if block_given?
    return visitor
  end

  def to_hash
    @attributes
  end

  def dump
    Oj.dump(@attributes, mode: :compat)
  end
end
