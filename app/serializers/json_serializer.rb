class JSONSerializer
  def initialize(serializers)
    @serializers = serializers
  end

  def serialize(object)
    visitor = JSONSerializerVisitor.new
    @serializers.each do |serializer|
      serializer.serialize(object, visitor)
    end

    visitor.dump
  end
end
