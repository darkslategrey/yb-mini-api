class BookWithReadingSerializer < Serializer
  def serialize(book, visitor)
    visitor[:reading] = reading(book)
  end

  private

  def reading(book)
    {
      progress: book.reading_progress.to_f,
      added_to_library: book.added_to_library == 't'.freeze
    }
  end
end

