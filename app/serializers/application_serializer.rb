class ApplicationSerializer < Serializer
  def serialize(application, visitor)
      visitor[:version]              = application.version
      visitor[:last_connection_date] = application.last_connection_date
      visitor[:marketing_name]       = application.marketing_name
      visitor[:os]                   = application.os
      visitor[:os_version]           = application.os_version
      visitor[:screen_width]         = application.screen_width
      visitor[:screen_height]        = application.screen_height
      visitor[:apns_token]           = application.apns_token
      visitor[:c2dma_token]          = application.c2dma_token
  end
end

