class BookListSerializer < Serializer
  def serialize(book_list, visitor)
    visitor[:id]                = book_list.id
    visitor[:name]              = book_list.name
    visitor[:books_count]       = book_list.books_count
    visitor[:deletable]         = deletable?(book_list)
    visitor[:editable]          = editable?(book_list)
    visitor[:offline_available] = book_list.offline_available
  end

  private

  def deletable?(book_list)
    @context.permissions.allowed_to_delete_book_list?(book_list)
  end

  def editable?(book_list)
    @context.permissions.allowed_to_edit_book_list?(book_list)
  end
end
