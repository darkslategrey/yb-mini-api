class BookReadingSerializer < Serializer

  def serialize(book_reading, visitor)
    visitor[:added_to_library]       = book_reading.added_to_favorite
    visitor[:progress]               = book_reading.progress.to_f
    visitor[:rental_expiration_date] = nil
    visitor[:advertising_targetting] = advertising_targetting(book_reading)
    visitor[:facebook_targetting]    = facebook_targetting(book_reading.book)
    visitor[:liked]                  = book_reading.liked
    visitor[:book]                   = book_hash(book_reading.book, visitor)
  end

  private

  def book_hash(book, visitor)
    visitor.nested do |book_visitor|
      serializer = BookSerializer.new(@context)
      serializer.serialize(book, book_visitor)
    end
  end

  def advertising_targetting(book_reading)
    book = book_reading.book
    result = []
    result << book_reading.user_ipad.advertising_string
    result << "book=#{book.id}"
    result << "editor=#{book.editor_id}"
    result << "categories=#{book.categories_mapping.join(',')}"
    result.join(";")
  end

  def facebook_targetting(book)
    book.categories_mapping.join(',')
  end
end

