require 'i18n'

class Serializer
  extend Forwardable
  def_delegator :I18n, :translate, :t

  def initialize(context)
    @context = context
  end
end
