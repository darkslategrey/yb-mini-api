class AffiliateSerializer < Serializer
  def serialize(affiliate, visitor)
    visitor[:affiliate_link] = affiliate.affiliate_link
  end
end
