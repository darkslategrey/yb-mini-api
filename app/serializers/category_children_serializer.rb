class CategoryChildrenSerializer < Serializer
  def serialize(category, visitor)
    visitor[:children] = children(category, visitor)
  end

  private

  def children(category, visitor)
    child_serializer = CategorySerializer.new(@context)
    category.children_with_api_fields.map do |child_category|
      visitor.nested do |child_visitor|
        child_serializer.serialize(child_category, child_visitor)
      end
    end
  end
end

