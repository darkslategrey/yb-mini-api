class CategorySerializer < Serializer
  def serialize(category, visitor)
    visitor[:id]                     = category.id
    visitor[:title]                  = category.name
    visitor[:img_landscape_1024_url] = category.img_landscape_1024_url
    visitor[:img_landscape_568_url]  = category.img_landscape_568_url
    visitor[:img_square_485_url]     = category.img_square_485_url
  end
end

