# encoding: utf-8
class BooksController < BaseController

  before_filter :authenticate_user!, except: [:affiliate, :book_category, :book_tribe, :search, :top, :recents, :suggestions, :from_same_authors]

  before_filter :set_cache_headers, only: [:show, :recents, :suggestions, :from_same_authors]
  #caches_action :recents, :suggestions, :from_same_authors, :show, cache_path: self.book_cache_path, expires_in: 1.hour

  def index
    @books = Book.all
    render_json(@books, [BookSerializer])
  end

  def create
    @book = Book.new(params[:book])
    if @book.save
      render_json(@book, [BookSerializer])
    else
      render_json({code: 23, message: "Cannot save book <#{@book.errors}"},
                  [FormErrorSerializer])
    end
  end

  def destroy
    @book = Book.find(params[:id])
    @book.destroy
    render_json(@book, [BookSerializer])
  end

  def update
  end

  def top
    @books = if current_user
               #TODO: filter by book_filter
               UserIpad::Recommender.get_user_top_books(current_user).select_default_api_fields
             else
               content_provider.books.api_top.select_default_api_fields.preload(:editor, :authors)
             end
    render_json(@books, [BookSerializer], paginate: true)
  end

  def recents
    selection = content_provider.selections.find_by_unique_identifier(Selection::NOUVEAUTES_UUID)
    @books = selection.books.order("order_position DESC").select_default_api_fields.includes(:editor, :authors)
    render_json(@books, [BookSerializer], paginate: true)
  end

  def show
    #@book = content_provider.books_accessible_by_user.select_default_api_fields.find(params[:id])
    @book = Book.find(params[:id])
    render_json(@book, [ShowBookSerializer])
  end

  def affiliate
    book = content_provider.books_accessible_by_user.find(params[:id])
    affiliate = Affiliate.new(book, current_user)
    render_json(affiliate, [AffiliateSerializer])
  end

  def search
    no_porn = params[:is_adult_filtered] == '1'
    page    = (params[:page] || 1).to_i - 1
    country = current_user.country if current_user
    country ||= 'FR'
    books   = YB::Core.search_engine.books.search(params[:keywords], page: page,
                                                                     per: params[:per],
                                                                     catalog_id: context.sub_application.catalog_id,
                                                                     no_porn: no_porn,
                                                                     country: country)
    @books = books.includes(:authors, :editor).select_default_api_fields
    render_json(@books, [BookSerializer])
  rescue InvalidSearchQueryError => error
    render json: {error: error.message}
  end

  def from_same_authors
    params[:page] ||= 1
    params[:per] ||= 12
    book = content_provider.books.select('books.id').find(params[:id])
    @books = content_provider.books
      .select_default_api_fields
      .select('books.visible_at')
      .includes(:editor, :authors)
      .joins(:authors_books)
      .where("books.id <> ? AND authors_books.author_id IN (?)", book.id, book.author_ids)
      .order("books.visible_at DESC")
      .uniq
    render_json(@books, [BookSerializer], paginate: true)
  end

  def suggestions
    book   = content_provider.books.find(params[:id])
    books  = content_provider.books.select_default_api_fields.includes(:authors, :editor)
    @books = BookSuggestions.new(books, book, request_cache_key)
    render_json(@books, [BookSerializer], paginate: true)
  end


end
