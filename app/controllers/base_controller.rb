class BaseController < ApplicationController
  extend Forwardable
  def_delegators :context, :library, :content_provider, :permissions,
                 :current_device_type

  respond_to :json

  #Should be increment when you want to invalidate API cache
  API_MINOR_VERSION = 1

  #
  # Catch ActiveRecord::RecordInvalid and return a properly
  # formatted api error
  #
  # Add around_filter :catch_record_invalid to enable it
  # on a method
  #
  def catch_record_invalid
    yield
  rescue ActiveRecord::RecordInvalid => e
    invalid_record_error(e.record)
  end

  def build_record_error(record)
    fields = record.errors.inject({}) do |memo, error|
      attribute, error_msg = error
      memo[attribute]      = record.errors.full_message(attribute, error_msg)
      memo
    end

    invalid_fields_error(fields)
  end

  def invalid_fields_error(fields)
    FormError.new(code: 400,
                  message: "The form contains invalid parameters",
                  fields: fields)
  end

  def invalid_record_error(record)
    error = build_record_error(record)
    invalid_form_error(error)
  end

  def invalid_form_error(error)
    render_json(error, [FormErrorSerializer], status: 403)
  end

  def check
    render json: {status: "ok"}
  end

  alias_method :up, :check

  def paginate_collection(collection)
    params[:page] ||= 1
    params[:per]  ||= 12
    params[:page]   = 100 if params[:page].to_i > 100

    if collection.respond_to?(:page)
      collection.page(params[:page]).per(params[:per])
    else
      Kaminari.paginate_array(collection)
    end
  end


  def render_json(object, serializers, opts = {})
    status = opts.delete(:status) || 200
    serializers.map! { |serializer|
      serializer.is_a?(Class) ? serializer.new(context) : serializer
    }
    serializer = if object.is_a?(ActiveRecord::Relation) || object.is_a?(Enumerable)
                   object = paginate_collection(object) if opts[:paginate]
                   JSONArraySerializer.new(serializers, root: opts[:root])
                 else
                   JSONSerializer.new(serializers)
                 end

    render json: serializer.serialize(object),
           status: status
  end

  def set_cache_headers
    expires_in 1.hour, public: true
  end

  private

  def context
    RequestContext.new(request: request)
  end

  def render_errors(errors)
    json ={ success: false, errors: errors }
    render json: json, status: :bad_request
  end



  #def authenticate_user!
  #  render json: {error: 'authentication error'}, status: 401 unless current_user
  #end




end
